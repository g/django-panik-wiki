import combo.public.views
from django.urls import path, re_path

from . import views

urlpatterns = [
    path('newpage/', views.new_page),
    path('ajax/newpage/', views.ajax_new_page),
    path('ajax/upload/', views.ajax_upload),
    path('ajax/make-public/<int:pk>/', views.make_public, name='wiki-make-public'),
    path('ajax/make-private/<int:pk>/', views.make_private, name='wiki-make-private'),
    re_path(
        r'^ajax/wiki/(?P<page_pk>\d+)/cell/(?P<cell_reference>[\w_-]+)/$',
        views.edit_wiki,
        name='wiki-edit',
    ),
    re_path(r'^', combo.public.views.page),
]
