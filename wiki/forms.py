from django import forms
from django.utils.translation import gettext_lazy as _


class NewPageForm(forms.Form):
    title = forms.CharField(label=_('Title'), required=True)
