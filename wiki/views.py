import os

from combo.data.models import Page, PageSnapshot, TextCell
from combo.manager.views import PageEditCellView
from django.contrib.auth.decorators import login_required
from django.core.files.storage import default_storage
from django.db.models import Q
from django.http import HttpResponseForbidden, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.edit import FormView
from sorl.thumbnail.shortcuts import get_thumbnail

from .forms import NewPageForm


class NewPageView(FormView):
    form_class = NewPageForm
    template_name = 'wiki/new_page.html'

    def form_valid(self, form):
        parent_wiki_page = Page.objects.get(slug='wiki', parent=None)
        self.new_page = Page(title=form.cleaned_data['title'], parent=parent_wiki_page, public=False)
        self.new_page.save()
        cell = TextCell(placeholder='content', page=self.new_page, order=0)
        cell.text = '<p>Nouvelle page, contenu à ajouter</p>'
        cell.save()
        return super().form_valid(form)

    def get_success_url(self):
        return self.new_page.get_online_url()


new_page = login_required(NewPageView.as_view())


@csrf_exempt
@login_required
def ajax_new_page(request, *args, **kwargs):
    parent_wiki_page = Page.objects.get(slug='wiki', parent=None)
    title = request.POST['title']
    slug = slugify(title)
    page = Page.objects.filter(Q(slug=slug) | Q(title=title)).first()
    if not page:
        page = Page(title=title, parent=parent_wiki_page, public=False)
        page.save()
        cell = TextCell(placeholder='content', page=page, order=0)
        cell.text = '<p>Nouvelle page, contenu à ajouter</p>'
        cell.save()
    return JsonResponse(
        {
            'url': page.get_online_url(),
            'request_id': request.POST['request_id'],
        }
    )


@csrf_exempt
@login_required
def ajax_upload(request, *args, **kwargs):
    upload = request.FILES['upload']
    upload_path = 'uploads'
    if os.path.splitext(upload.name.lower())[-1] in ('.jpg', '.jpeg', '.png', '.gif', '.svg'):
        upload_path = 'images'
    saved_path = default_storage.save('wiki/%s/%s' % (upload_path, upload.name), upload)
    url = '/media/' + saved_path
    response = {'url': url, 'filename': upload.name}
    if upload_path == 'images':
        if default_storage.size(saved_path) > 500_000 and not upload.name.endswith('.svg'):
            response['orig_url'] = url
            try:
                response['url'] = get_thumbnail(saved_path, '1000', upscale=False).url
            except OSError:
                pass
    return JsonResponse(response)


@login_required
def make_public(request, *args, **kwargs):
    page = Page.objects.get(id=kwargs['pk'])
    page.public = True
    page.save()
    return HttpResponseRedirect(page.get_online_url())


@login_required
def make_private(request, *args, **kwargs):
    page = Page.objects.get(id=kwargs['pk'])
    page.public = False
    page.save()
    return HttpResponseRedirect(page.get_online_url())


class WikiEdit(PageEditCellView):
    def dispatch(self, request, *args, **kwargs):
        if not request.user or request.user.is_anonymous:
            return HttpResponseForbidden()
        self.page = get_object_or_404(Page, id=kwargs.get('page_pk') or kwargs.get('pk'))
        pages = self.page.get_parents_and_self()
        if not pages[0].slug == 'wiki':
            return HttpResponseForbidden()
        cell = self.get_object()
        new_text = request.POST['c%s-text' % cell.get_reference()]
        if new_text != cell.text:
            cell.text = request.POST['c%s-text' % cell.get_reference()]
            cell.save()
            PageSnapshot.take(self.page, request, comment=_('changed wiki text'))
        return JsonResponse({'err': 0})


edit_wiki = WikiEdit.as_view()
