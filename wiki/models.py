# django-panik-wiki - custom wiki
# Copyright (C) 2020  Frederic Peters
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import urllib.parse

from combo.data.models import Page, TextCell
from django.db import models, transaction
from django.db.models.signals import post_save
from django.dispatch import receiver


class Interlink(models.Model):
    cell = models.ForeignKey(TextCell, on_delete=models.CASCADE)
    page = models.ForeignKey(Page, on_delete=models.SET_NULL, null=True)


@receiver(post_save, sender=TextCell)
def maintain_interlinks(sender, instance=None, **kwargs):
    with transaction.atomic():
        links = re.findall(r'href="(.*?)"', instance.text or '')
        Interlink.objects.filter(cell=instance).delete()
        for link in links:
            parsed = urllib.parse.urlparse(link)
            if parsed.netloc != '':
                continue
            if not parsed.path.startswith('/wiki/'):
                continue
            for page in Page.objects.filter(slug=parsed.path.strip('/').split('/')[-1]):
                if page.get_online_url() == parsed.path:
                    Interlink.objects.create(cell=instance, page=page)
                    break
